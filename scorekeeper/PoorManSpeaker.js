BASE.require([
  "BASE.async.Fulfillment"
], function(){
  BASE.namespace("scorekeeper");

  scorekeeper.PoorManSpeaker = function(){
    var self = this;
    var voice = null;

    speechSynthesis.onvoiceschanged = function(){
      var allVoices = speechSynthesis.getVoices();
      voice = allVoices.filter(function(v){
        return v.name == "Google UK English Female";
      })[0];
    };

    self.speakAsync = function(message){
      var fulfillment = new BASE.async.Fulfillment();
      var utterance = new SpeechSynthesisUtterance();

      utterance.voice = voice;

      utterance.onend = function(){
        fulfillment.setValue();
      };

      utterance.onerror = function(error){
        fulfillment.setError(error);
      };

      utterance.text = message;
      speechSynthesis.speak(utterance);
      return fulfillment;
    };

  };
});
