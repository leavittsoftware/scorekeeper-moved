BASE.require([
  "BASE.util.Observable"
], function(){
  BASE.namespace("scorekeeper");

  scorekeeper.Game = function(players){
    var self = this;
    BASE.util.Observable.apply(self);
    var winningScore = 11;
    var winByMargin = 2;
    var scores = {a:0, b:0};
    var gameOver = false;
    var server = "a";
    var serves = 0;

    if (typeof players !== "object" || typeof players.a === "undefined" || typeof players.b === "undefined") {
      throw new Error("Game needs a player 'a' and a player 'b'");
    }

    self.point = function(player){
      if (gameOver) {
        return;
      }

      if (player === players.a) {
        scores.a += 1;
      } else if (player === players.b) {
        scores.b += 1;
      } else {
        throw new Error("Point awarded to invalid player");
      }
      self.notify({type:"point", player:player, scoreA:scores.a, scoreB:scores.b});

      if (scores.a >= winningScore && (scores.a - scores.b) >= winByMargin) {
        gameOver = true;
        self.winner = players.a;
        self.notify({type:"end", winner:players.a});
      } else if (scores.b >= winningScore && (scores.b - scores.a) >= winByMargin) {
        gameOver = true;
        self.winner = players.b;
        self.notify({type:"end", winner:players.b});
      }

      if (!gameOver) {
        serve();
      }
    };

    var serve = function(){
      self.notify({type:"serve", player:players[server]});
      serves += 1;
      if (serves >= 2) {
        if (server === "a") {
          server = "b";
        } else {
          server = "a";
        }
        serves = 0;
      }
    };

    self.start = function(){
      serve();
    };

  };
});
