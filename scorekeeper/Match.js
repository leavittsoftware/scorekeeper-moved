BASE.require([
  'BASE.util.Observable',
  'BASE.util.Observer',
  'scorekeeper.Game'
], function(){
  BASE.namespace("scorekeeper");

  scorekeeper.Match = function(options){
    var self = this;
    BASE.util.Observable.apply(self);
    options = options || {};
    options.numberOfGames = options.numberOfGames || 3;
    options.players = options.players || {};
    options.players.a = options.players.a || "A";
    options.players.b = options.players.b || "B";
    var games = [];
    var gameObserver = new BASE.util.Observer();

    var currentGameIndex = 0;

    for (var i = 0; i < options.numberOfGames; i++) {
      games.push(new scorekeeper.Game(options.players));
    }

    self.point = function(player){
      // player should be a reference to the player provided in options
      // if no player was give in options, player should be "A" or "B"
      games[currentGameIndex].point(player);
    };

    self.start = function(){
      startGame(games[currentGameIndex]);
      self.notify({type:"matchStart"});
    };

    self.activeGame = null;

    var startGame = function(game){
      self.notify({type:"gameStart", gameNumber:(currentGameIndex+1)});

      gameObserver.dispose();
      gameObserver = game.observe();

      gameObserver.filter(function(event){
        return event.type === "point";
      }).onEach(function(event){
        self.notify(event);
      });

      gameObserver.filter(function(event){
        return event.type === "end";
      }).onEach(function(event){
        event.type = "gameEnd";
        self.notify(event);
        nextGame();
      });

      gameObserver.filter(function(event){
        return event.type === "serve";
      }).onEach(function(event){
        self.notify(event);
      });

      game.start();
    };

    var nextGame = function(){
      currentGameIndex++;
      var playerAWins = games.filter(function(g){ return g.winner === options.players.a; }).length;
      var playerBWins = games.filter(function(g){ return g.winner === options.players.b; }).length;
      var majorityWinner = Math.max(playerAWins, playerBWins) > options.numberOfGames / 2;

      if (currentGameIndex >= options.numberOfGames || majorityWinner) {
        var winner = null;
        var draw = false;
        if (playerAWins > playerBWins) {
          winner = options.players.a;
        } else if (playerBWins > playerBWins) {
          winner = options.players.b;
        } else {
          draw = true;
        }

        self.notify({type:"matchEnd", draw:draw, winner:winner});
        self.activeGame = null;
      } else {
        startGame(games[currentGameIndex]);
        self.activeGame = games[currentGameIndex];
      }
    };

  };

  scorekeeper.Match.playerFactory = function(playerA, playerB) {
    var players = {};
    players.a = playerA;
    players.b = playerB;
    return players;
  };

});
