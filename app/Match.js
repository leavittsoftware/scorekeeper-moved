BASE.require([
  'jQuery',
  'BASE.util.Observer',
  'scorekeeper.Match',
  //'BASE.speech.WebSpeechSynthesis'
  'scorekeeper.PoorManSpeaker'
], function(){
  BASE.namespace("app");

  var SpeechSynthesis = scorekeeper.PoorManSpeaker;
  // var SpeechSynthesis = BASE.speech.WebSpeechSynthesis;

  app.Match = function(elem, tags, scope){
    var self = this;
    var match = null;
    var players = {};
    var matchObserver = new BASE.util.Observer();
    var speaker = new SpeechSynthesis();

    var $leftPlayerName = $(tags['left-player-name']);
    var $leftPlayerScore = $(tags['left-player-score']);
    var $rightPlayerName = $(tags['right-player-name']);
    var $rightPlayerScore = $(tags['right-player-score']);

    var $server = $(tags['server']);

    // speaker.getVoicesAsync().chain(function (voices) {
    //     filteredVoices = voices.filter(function (voice) {
    //         return voice.lang === "en-GB";
    //     });
    //     speaker.voice = filteredVoices[0];
    // })["try"]();

    var pointMessages = [
      "Point for #player#!",
      "#player# scores!",
      "The ball is afraid of #player#",
      "Go home #otherplayer#, #player# has your number.",
      "#otherplayer# never had a chance",
      "#otherplayer# throws a ball out of bounds exception",
      "#player# makes a point",
      "#otherplayer# is glad computers are so bad at smack talk",
      "#player# for the win!",
      "Somebody got a point.",
      "I am Vulcan and have no emotion!",
      "Hooray #player#",
      "#player# slams it like a Borg",
      "That ball did the kessel run in under 12 parsecs",
      "Whoopie kai yai yay"
    ];

    var winMessages = [
      "#player# wins!",
      "The game goes to #player#",
      "#player# has won the game and is obviously better than #otherplayer#"
    ];

    var matchWinMessages = [
      "#player# has won the match!",
      "Match over! #otherplayer# leaves the room, head low in shame.",
      "#player# takes the cake! By cake I mean the match!",
      "That's the final segmentation fault for #otherplayer#, and #player# is the champion"
    ];

    var prepareMessage = function(message, playerName){
      var otherPlayer = players.b;
      if (playerName == players.b) {
        otherPlayer = players.a;
      }
      return message.replace("#player#", playerName).replace("#otherplayer#", otherPlayer);
    };

    var $playerAScoreLabel = $leftPlayerScore;
    var $playerANameLabel = $leftPlayerName;
    var $playerBScoreLabel = $rightPlayerScore;
    var $playerBNameLabel = $rightPlayerName;
    var playerAOnLeft = true;

    self.setup = function(value){
      if (typeof value.match !== "object" || typeof value.match.observe === "undefined") {
        throw new Error("Match component needs a match when setting up.");
      }
      if (typeof value.players !== "object") {
        throw new Error("Need players when setting up match component.");
      }

      match = value.match;
      players = value.players;
      matchObserver.dispose();
      matchObserver = match.observe();

      $playerANameLabel.text(players.a);
      $playerBNameLabel.text(players.b);

      matchObserver.filter(function(event){
        return event.type === "point";
      }).onEach(function(event){
        $playerAScoreLabel.text(event.scoreA);
        $playerBScoreLabel.text(event.scoreB);
        if (Math.random() > 0.6) {
          var message = pointMessages[Math.floor(Math.random()*pointMessages.length)];
          message = prepareMessage(message, event.player);
          speaker.speakAsync(message).try();
        }
      });

      matchObserver.filter(function(event){
        return event.type === "serve";
      }).onEach(function(event){
        $server.text(event.player);
        var message = event.player +"'s serve.";
        speaker.speakAsync(message).try();
      });

      matchObserver.filter(function(event){
        return event.type === "gameEnd";
      }).onEach(function(event){
        var message = winMessages[Math.floor(Math.random()*winMessages.length)];
        message = prepareMessage(message, event.winner);
        $leftPlayerScore.text(0);
        $rightPlayerScore.text(0);
        speaker.speakAsync(message).try();
        if (playerAOnLeft) {
          playerAOnLeft = false;
          $playerAScoreLabel = $rightPlayerScore;
          $playerANameLabel = $rightPlayerName;
          $playerBScoreLabel = $leftPlayerScore;
          $playerBNameLabel = $leftPlayerName;
        } else {
          playerAOnLeft = true;
          $playerAScoreLabel = $leftPlayerScore;
          $playerANameLabel = $leftPlayerName;
          $playerBScoreLabel = $rightPlayerScore;
          $playerBNameLabel = $rightPlayerName;
        }
        $playerANameLabel.text(players.a);
        $playerBNameLabel.text(players.b);
      });

      matchObserver.filter(function(event){
        return event.type === "matchEnd";
      }).onEach(function(event){
        var message = matchWinMessages[Math.floor(Math.random()*matchWinMessages.length)];
        message = prepareMessage(message, event.winner);
        speaker.speakAsync(message).try();
      });
    }; // self.setup

  };
});
