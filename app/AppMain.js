BASE.require([
  'jQuery',
  'scorekeeper.Match'
], function(){
  var Match = scorekeeper.Match;
  BASE.namespace("app");

  app.AppMain = function(elem, tags, scope){
    var self = this;
    var keyEventEmitter = document;
    var $matchContainer = $(tags['match-container']);
    var matchContainerController = $matchContainer.controller();
    var $playerAName = $(tags['player-a-name']);
    var $playerBName = $(tags['player-b-name']);
    var $startMatchButton = $(tags['start-match']);
    var pointEventLockoutTime = 1500;

    var observers = [];

    var players = null;
    var match = null;
    var leftPlayer = null;
    var rightPlayer = null;

    var leftPlayerKeys = "qazwsxedcrfv";
    var leftPlayerKeyCodes = Array.prototype.map.call(leftPlayerKeys, function(c){return c.charCodeAt(0);});
    var rightPlayerKeys = "ujmik,ol.p;/";
    var rightPlayerKeyCodes = Array.prototype.map.call(rightPlayerKeys, function(c){return c.charCodeAt(0);});

    var lockOutPointEvents = function(){
      keypressHandler = keypressHandlerInactive;
      setTimeout(function(){
        keypressHandler = keypressHandlerActive;
      }, pointEventLockoutTime);
    };

    var isLeftPlayerPoint = function(event){
      return leftPlayerKeyCodes.indexOf(event.keyCode) > -1;
    };

    var isRightPlayerPoint = function(event){
      return rightPlayerKeyCodes.indexOf(event.keyCode) > -1;
    };

    var keypressHandlerActive = function(event){
      if (isLeftPlayerPoint(event)) {
        match.point(leftPlayer);
        lockOutPointEvents();
      } else if(isRightPlayerPoint(event)) {
        match.point(rightPlayer);
        lockOutPointEvents();
      }
    };

    var keypressHandlerInactive = function(event){};

    var keypressHandler = keypressHandlerInactive;

    $(document).on("keypress", function(event){
      keypressHandler(event);
    });

    $startMatchButton.on("click", function(){
      $startMatchButton.hide();

      observers.forEach(function(observer){
        observer.dispose();
      });

      players = Match.playerFactory($playerAName.val(), $playerBName.val());
      leftPlayer = players.a;
      rightPlayer = players.b;
      match = new Match({players:players, numberOfGames:3});

      observers.push(match.observeType("gameEnd", function(event){
        if (leftPlayer === players.a) {
          leftPlayer = players.b;
          rightPlayer = players.a;
        } else {
          leftPlayer = players.a;
          rightPlayer = players.b;
        }
      }));

      observers.push(match.observeType("matchEnd", function(event){
        $startMatchButton.show();
        keypressHandler = keypressHandlerInactive;
      }));

      observers.push(match.observeType("matchStart", function(event){
        keypressHandler = keypressHandlerActive;
      }));


      matchContainerController.setup({match:match, players:players});

      match.start();
    });

  };
});
